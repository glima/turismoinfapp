
import { HTTP } from '@ionic-native/http';
import { ToastController, App } from 'ionic-angular';
import { Injectable } from '@angular/core';

import { Loading } from '../../components/loading';

@Injectable()
export class RestTurismoProvider {

  //public static host: string = 'http://173.236.57.210:9595';
  public static host: string = 'http://192.168.25.17:9595';
  public static photoUrl: string = "/api/client/places/photos/";
  public static photoUrlEvent: string = "/api/client/events/photos/";
  public static photoUrlCategory: string = "/api/client/categories/photo/";

  constructor(public http: HTTP, public toastCtrl: ToastController, public loading: Loading, public app: App) {
    console.log('Hello RestTurismoProvider Provider');
  }

  changePass(data, successCallback, errorCallback) {
    this.http.setDataSerializer('json');

    this.http.put(RestTurismoProvider.host + '/api/client/user/password', data,
    {
      'Content-Type': 'application/json',
      'Authorization': 'Bearer ' + window.localStorage.getItem('logged')
    })
    .then(data => {
      successCallback(this.getMessage(data));
    })
    .catch(error => {
      this.verifyError(error, errorCallback);
    });
  }

  resetPass(email, successCallback, errorCallback) {
    this.http.setDataSerializer('json');

    this.http.put(RestTurismoProvider.host + '/api/client/user/forgotten_password', { 'email': email },
    {
      'Content-Type': 'application/json'
    })
    .then(data => {
      successCallback(this.getMessage(data));
    })
    .catch(error => {
      this.verifyError(error, errorCallback);
    });
  }

  loginFacebook(data, successCallback, errorCallback) {
    this.http.setDataSerializer('json');

    this.http.post(RestTurismoProvider.host + '/auth/login/facebook', data,
    {
      'Content-Type': 'application/json'
    })
    .then(data => {
      successCallback(JSON.parse(data.data));
    })
    .catch(error => {
      this.verifyError(error, errorCallback);
    });
  }

  login(data, successCallback, errorCallback) {
    this.http.setDataSerializer('json');

    this.http.post(RestTurismoProvider.host + '/auth/login', data,
    {
      'Content-Type': 'application/json'
    })
    .then(data => {
      successCallback(JSON.parse(data.data));
    })
    .catch(error => {
      this.verifyError(error, errorCallback);
    });
  }

  newUser(data, successCallback, errorCallback) {
    this.http.setDataSerializer('json');

    this.http.post(RestTurismoProvider.host + '/api/client/user', data,
    {
      'Content-Type': 'application/json'
    })
    .then(data => {
      successCallback(this.getMessage(data));
    })
    .catch(error => {
      this.verifyError(error, errorCallback);
    });
  }

  listCategories(successCallback, errorCallback) {
    this.http.setDataSerializer('json');

    this.http.get(RestTurismoProvider.host + '/api/client/categories', {},
    {
      'Authorization': 'Bearer ' + window.localStorage.getItem('logged')
    })
    .then(data => {
      successCallback(JSON.parse(data.data));
    })
    .catch(error => {
      this.verifyError(error, errorCallback);
    });
  }

  getInitialConfig(successCallback, errorCallback) {
    this.http.setDataSerializer('json');

    this.http.get(RestTurismoProvider.host + '/api/client/config/initial', {},
    {
      'Authorization': 'Bearer ' + window.localStorage.getItem('logged')
    })
    .then(data => {
      successCallback(JSON.parse(data.data));
    })
    .catch(error => {
      this.verifyError(error, errorCallback);
    });
  }

  getContactConfig(successCallback, errorCallback) {
    this.http.setDataSerializer('json');

    this.http.get(RestTurismoProvider.host + '/api/client/config/contact', {},
    {
      'Authorization': 'Bearer ' + window.localStorage.getItem('logged')
    })
    .then(data => {
      successCallback(JSON.parse(data.data));
    })
    .catch(error => {
      this.verifyError(error, errorCallback);
    });
  }

  listPlaces(offset, category, successCallback, errorCallback) {
    this.http.setDataSerializer('json');

    this.http.get(RestTurismoProvider.host + '/api/client/categories/' + category + '/places?page=' + offset, {},
    {
      'Authorization': 'Bearer ' + window.localStorage.getItem('logged')
    })
    .then(data => {
      successCallback(JSON.parse(data.data));
    })
    .catch(error => {
      this.verifyError(error, errorCallback);
    });
  }

  listEvents(offset, successCallback, errorCallback) {
    this.http.setDataSerializer('json');

    this.http.get(RestTurismoProvider.host + '/api/client/events?page=' + offset, {},
    {
      'Authorization': 'Bearer ' + window.localStorage.getItem('logged')
    })
    .then(data => {
      successCallback(JSON.parse(data.data));
    })
    .catch(error => {
      this.verifyError(error, errorCallback);
    });
  }

  listPlacePhotos(place, successCallback, errorCallback) {
    this.http.setDataSerializer('json');

    this.http.get(RestTurismoProvider.host + '/api/client/places/' + place + '/photos', {},
    {
      'Authorization': 'Bearer ' + window.localStorage.getItem('logged')
    })
    .then(data => {
      successCallback(JSON.parse(data.data));
    })
    .catch(error => {
      this.verifyError(error, errorCallback);
    });
  }

  listEventPhotos(event, successCallback, errorCallback) {
    this.http.setDataSerializer('json');

    this.http.get(RestTurismoProvider.host + '/api/client/events/' + event + '/photos', {},
    {
      'Authorization': 'Bearer ' + window.localStorage.getItem('logged')
    })
    .then(data => {
      successCallback(JSON.parse(data.data));
    })
    .catch(error => {
      this.verifyError(error, errorCallback);
    });
  }

  fail() {
    let toast = this.toastCtrl.create({
      message: 'Ocorreu um erro de conexão com o servidor. Tente novamente mais tarde.',
      duration: 5000
    });
    toast.present();
  }

  getMessage(data) {
    try {
      let msg = JSON.parse(data.data);
      let textM = '';
        for(let i = 0; i < msg.length; i++) {
          textM += msg[i] + ' ';
        }

        return data;
    } catch (error) {
      return data.data;
    }
  }

  verifyError(error, errorCallback) {
    if(error.status == 500 || error.status == -1) {
      Loading.dismissLoading();
      this.fail();
    } else if(error.status == 401) {
      let nav = this.app.getActiveNav();

      window.localStorage.setItem('logged', null);

      nav.setRoot('AuthPage');
      nav.popToRoot();
    } else {
      if(error.message) {
        try {
          let msg = JSON.parse(error.message);
          if(msg instanceof Array) {
            let textM = '';
            for(let i = 0; i < msg.length; i++) {
              textM += msg[i] + ' ';
            }

            error.message = textM;
          } else {
            error.message = msg;
          }
        } catch (error) {

        }

        errorCallback(error);
      } else {
        try {
          let msg = JSON.parse(error.error);
          if(msg instanceof Array) {
            let textM = '';
            for(let i = 0; i < msg.length; i++) {
              textM += msg[i] + ' ';
            }

            error.message = textM
          } else {
            error.message = msg;
          }
        } catch (errorEx) {
          error.message = error.error;
        }

        errorCallback(error);
      }
    }
  }
}
