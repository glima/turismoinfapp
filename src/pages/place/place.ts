import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { HTTP } from '@ionic-native/http';
import { ImageLoader, ImageLoaderConfig } from 'ionic-image-loader';
import { HttpHeaders } from '@angular/common/http';

import { PlaceDetailsPage } from '../place-details/place-details';
import { RestTurismoProvider } from '../../providers/rest-turismo/rest-turismo';
import { Loading } from '../../components/loading';

@IonicPage()
@Component({
  selector: 'page-place',
  templateUrl: 'place.html',
})
export class PlacePage {

  places: any = [];
  offset: any;
  host: any = RestTurismoProvider.host + RestTurismoProvider.photoUrl;

  constructor(public navCtrl: NavController, public navParams: NavParams, private http: HTTP,
    private restTurismo: RestTurismoProvider, public loading: Loading, private imageLoaderConfig: ImageLoaderConfig) {

      this.imageLoaderConfig.setHttpHeaders(
        new HttpHeaders().append('Authorization', 'Bearer ' + window.localStorage.getItem('logged'))
      );

      this.offset = 0;
      Loading.showLoading();
      this.listPlaces();
  }

  ionViewWillLeave() {
    Loading.dismissLoading();
  }

  ionViewDidLoad() {
    Loading.dismissLoading();
  }

  showPhoto(img) {
    return img;
  }

  listPlaces() {

    this.restTurismo.listPlaces(this.offset, this.navParams.get('category').id,
      (data) => {
        Loading.dismissLoading();
        if(data.length > 0) {
          this.places = this.places.concat(data);
          this.offset = this.offset + 1;
        }

      },
      (error) => {
        Loading.dismissLoading();
      }
    );
  }

  doInfinite() {
    return new Promise((resolve) => {
      setTimeout(() => {
        this.listPlaces();
        resolve();
      }, 1000);
    });
  }

  goPlaceDetails(place) {
    this.navCtrl.push('PlaceDetailsPage', {
      place: place
    });
  }

}
