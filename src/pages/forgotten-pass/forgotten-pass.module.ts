import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ForgottenPassPage } from './forgotten-pass';

@NgModule({
  declarations: [
    ForgottenPassPage,
  ],
  imports: [
    IonicPageModule.forChild(ForgottenPassPage),
  ],
})
export class ForgottenPassPageModule {}
