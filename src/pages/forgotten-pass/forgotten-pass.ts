import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ToastController } from 'ionic-angular';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

import { Loading } from '../../components/loading';
import { regexValidators } from '../validators/validator';
import { RestTurismoProvider } from '../../providers/rest-turismo/rest-turismo';


@IonicPage()
@Component({
  selector: 'page-forgotten-pass',
  templateUrl: 'forgotten-pass.html',
})
export class ForgottenPassPage {

  public credentialsForm: FormGroup;

  constructor(public navCtrl: NavController, public navParams: NavParams, public loading: Loading,
    private formBuilder: FormBuilder, private restTurismo: RestTurismoProvider, public toastCtrl: ToastController) {

      this.credentialsForm = this.formBuilder.group({

        email: [
          '',
          Validators.compose([Validators.pattern(regexValidators.email), Validators.required])
        ]
      });
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ForgottenPassPage');
  }

  resetPass() {
    Loading.showLoading();
    this.restTurismo.resetPass(this.credentialsForm.controls['email'].value,
      (data) => {
        Loading.dismissLoading();
        this.credentialsForm.reset();
        this.showMessage(data);
      },
      (error) => {
        Loading.dismissLoading();
        this.showMessage(error.message);
      }
    );
  }

  showMessage(msg) {
    let toast = this.toastCtrl.create({
      message: msg,
      duration: 3000
    });
    toast.present();
  }
}
