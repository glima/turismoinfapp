import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ToastController, LoadingController } from 'ionic-angular';
import { Facebook, FacebookLoginResponse } from '@ionic-native/facebook';
import { HTTP } from '@ionic-native/http';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

import { HomePage } from '../home/home';
import { SignupPage } from '../signup/signup';
import { ForgottenPassPage } from '../forgotten-pass/forgotten-pass';
import { RestTurismoProvider } from '../../providers/rest-turismo/rest-turismo';
import { Loading } from '../../components/loading';
import { regexValidators } from '../validators/validator';

@IonicPage()
@Component({
  selector: 'page-auth',
  templateUrl: 'auth.html',
})
export class AuthPage {

  public credentialsForm: FormGroup;

  constructor(public navCtrl: NavController, public navParams: NavParams, public toastCtrl: ToastController,
    private facebook: Facebook, private restTurismo: RestTurismoProvider, public loading: Loading,
    private formBuilder: FormBuilder) {

      this.credentialsForm = this.formBuilder.group({

        email: [
          '',
          Validators.compose([Validators.pattern(regexValidators.email), Validators.required])
        ],
        password: ['', Validators.required]
      });
  }

  ionViewWillLeave() {
    Loading.dismissLoading();
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad AuthPage');
  }

  enter() {
      Loading.showLoading();
      this.restTurismo.login(
        { username: this.credentialsForm.controls['email'].value, password: this.credentialsForm.controls['password'].value },
        (data) => {
          Loading.dismissLoading();
          this.successLogin(data.token);
        },
        (error) => {
          Loading.dismissLoading();

          if(error.status == 401) {
            this.failLogin('Usuário ou senha incorreto.');
          }
        }
      );
  }

  newUser() {
    this.navCtrl.push('SignupPage');
  }

  forgottenPass() {
    this.navCtrl.push('ForgottenPassPage');
  }

  loginFacebook() {
    let permissions = new Array<string>();
     permissions = ["public_profile", "email"];

     this.facebook.login(permissions).then((response) => {
      let params = new Array<string>();

      this.facebook.api("/me?fields=name,email", params)
      .then(res => {
          let data = { email: res.email, id: res.id, token: response.authResponse.accessToken };
          this.login(data);
      }, (error) => {
        console.log('ERRO LOGIN: ',error);
        this.failLogin("Erro de conexão. Tente novamente.");
      })
    }, (error) => {

    });

  }

  login(datafb) {
      Loading.showLoading();
      this.restTurismo.loginFacebook(datafb,
        (data) => {
          Loading.dismissLoading();
          this.successLogin(data.token);
        },
        (error) => {
          Loading.dismissLoading();
          this.failLogin(error.message);
        }
      );
  }

  successLogin(token) {
    window.localStorage.setItem('logged', token);

    this.navCtrl.setRoot('HomePage');
    this.navCtrl.popToRoot();
  }

  failLogin(msg) {
    let toast = this.toastCtrl.create({
      message: msg,
      duration: 3000
    });
    toast.present();
  }
}
