import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ToastController } from 'ionic-angular';
import { HTTP } from '@ionic-native/http';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

import { RestTurismoProvider } from '../../providers/rest-turismo/rest-turismo';
import { Loading } from '../../components/loading';
import { regexValidators } from '../validators/validator';

@IonicPage()
@Component({
  selector: 'page-signup',
  templateUrl: 'signup.html',
})
export class SignupPage {

  public credentialsForm: FormGroup;

  constructor(public navCtrl: NavController, public navParams: NavParams, private http: HTTP,
    private restTurismo: RestTurismoProvider, public toastCtrl: ToastController, public loading: Loading,
    private formBuilder: FormBuilder) {

      this.credentialsForm = this.formBuilder.group({

        email: [
          '',
          Validators.compose([Validators.pattern(regexValidators.email), Validators.required])
        ],
        password: ['', Validators.required],
        phone: [''],
        name: ['', Validators.required]
      });
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad SignupPage');
  }

  newUser() {
    Loading.showLoading();
    console.log(this.getData());
    this.restTurismo.newUser(this.getData(),
      (data) => {
        Loading.dismissLoading();

        let toast = this.toastCtrl.create({
          message: 'Cadastro realizado com sucesso.',
          duration: 3000
        });
        toast.present();

        this.navCtrl.popToRoot();
      },
      (error) => {
        Loading.dismissLoading();

        if(error.status == 400) {
          let msg = "";
          for(let i = 0; i < error.message.length; i++) {
            msg += error.message[i];
          }

          let toast = this.toastCtrl.create({
            message: error.message,
            duration: 3000
          });
          toast.present();
        }
      }
    );
  }

  getData() {
    return {
      'name': this.credentialsForm.controls['name'].value,
      'password': this.credentialsForm.controls['password'].value,
      'email': this.credentialsForm.controls['email'].value,
      'phone': this.credentialsForm.controls['phone'].value};
  }

}
