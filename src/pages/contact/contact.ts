import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ToastController } from 'ionic-angular';
import { CallNumber } from '@ionic-native/call-number';
import { EmailComposer } from '@ionic-native/email-composer';

import { RestTurismoProvider } from '../../providers/rest-turismo/rest-turismo';
import { Loading } from '../../components/loading';

@IonicPage()
@Component({
  selector: 'page-contact',
  templateUrl: 'contact.html',
})
export class ContactPage {

  config: any;

  constructor(public navCtrl: NavController, public navParams: NavParams, private callNumber: CallNumber,
    public toastCtrl: ToastController, private emailComposer: EmailComposer,
    private restTurismo: RestTurismoProvider, public loading: Loading) {
      this.config = { emailContact: '', phoneContact: '' };
      this.loadConfig();
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ContactPage');
  }

  loadConfig() {
    Loading.showLoading();

    this.restTurismo.getContactConfig(
      (data) => {
        Loading.dismissLoading();
        this.config = data;

      },
      (error) => {
        Loading.dismissLoading();
      }
    );
  }

  callTo() {
    this.callNumber.callNumber(this.config.phoneContact, true)
      .then(res => console.log('Launched dialer!', res))
      .catch(err => {
        let toast = this.toastCtrl.create({
          message: 'Erro ao abrir o aplicativo de chamada.',
          duration: 3000
        });
        toast.present();

        console.log('Error launching dialer', err)
      });
  }

  sendEmail() {
    let email = {
      to: this.config.emailContact,
      subject: 'Contato',
      isHtml: true
    };

    // Send a text message using default options
    this.emailComposer.open(email);

  }

}
