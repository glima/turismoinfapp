import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ViewController } from 'ionic-angular';
import { ImageLoader, ImageLoaderConfig } from 'ionic-image-loader';
import { HttpHeaders } from '@angular/common/http';

import { RestTurismoProvider } from '../../providers/rest-turismo/rest-turismo';


@IonicPage()
@Component({
  selector: 'page-event-photos',
  templateUrl: 'event-photos.html',
})
export class EventPhotosPage {

  photos: any;
  host: any = RestTurismoProvider.host + RestTurismoProvider.photoUrlEvent;

  constructor(public navCtrl: NavController, public navParams: NavParams, public viewCtrl: ViewController,
    private imageLoaderConfig: ImageLoaderConfig) {
    this.photos = navParams.data;

    this.imageLoaderConfig.setHttpHeaders(
      new HttpHeaders().append('Authorization', 'Bearer ' + window.localStorage.getItem('logged'))
    );
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad EventPhotosPage');
  }

  dismiss() {
    this.viewCtrl.dismiss();
  }

}
