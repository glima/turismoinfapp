import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { IonicImageLoader } from 'ionic-image-loader';

import { EventPhotosPage } from './event-photos';

@NgModule({
  declarations: [
    EventPhotosPage,
  ],
  imports: [
    IonicPageModule.forChild(EventPhotosPage),
    IonicImageLoader
  ],
})
export class EventPhotosPageModule {}
