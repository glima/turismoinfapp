import { Component } from '@angular/core';
import { ModalController } from 'ionic-angular';
import { IonicPage, NavController, NavParams, ToastController } from 'ionic-angular';
import { CallNumber } from '@ionic-native/call-number';
import { EmailComposer } from '@ionic-native/email-composer';

import { PlacePhotosPage } from '../place-photos/place-photos';
import { RestTurismoProvider } from '../../providers/rest-turismo/rest-turismo';
import { Loading } from '../../components/loading';
import { LaunchNavigator, LaunchNavigatorOptions } from '@ionic-native/launch-navigator';


@IonicPage()
@Component({
  selector: 'page-place-details',
  templateUrl: 'place-details.html',
})
export class PlaceDetailsPage {

  place: any;

  constructor(public navCtrl: NavController, public navParams: NavParams, public modalCtrl: ModalController,
    private restTurismo: RestTurismoProvider, public loading: Loading, private callNumber: CallNumber,
    public toastCtrl: ToastController, private emailComposer: EmailComposer, private launchNavigator: LaunchNavigator) {

    this.place = navParams.get('place');
  }

  ionViewWillLeave() {
    Loading.dismissLoading();
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad PlaceDetailsPage');
  }

  openAddress() {
    let options: LaunchNavigatorOptions = {
      app: this.launchNavigator.APP.GOOGLE_MAPS
    };

    this.launchNavigator.navigate(this.place.street + ' ' + this.place.number + ', ' + this.place.city, options)
      .then(
        success => console.log('Launched navigator'),
        error => console.log('Error launching navigator', error)
      );
  }

  showPhotos() {
    Loading.showLoading();
    this.restTurismo.listPlacePhotos(this.place.id,
      (data) => {
        Loading.dismissLoading();

        let modal = this.modalCtrl.create('PlacePhotosPage', data);
        modal.present();
      },
      (error) => {
        Loading.dismissLoading();
      }
    );
  }

  callTo() {
    this.callNumber.callNumber(this.place.phone, true)
      .then(res => console.log('Launched dialer!', res))
      .catch(err => {
        let toast = this.toastCtrl.create({
          message: 'Erro ao abrir o aplicativo de chamada.',
          duration: 3000
        });
        toast.present();

        console.log('Error launching dialer', err)
      });
  }

  sendEmail() {
    let email = {
      to: this.place.email,
      subject: 'Contato',
      isHtml: true
    };

    // Send a text message using default options
    this.emailComposer.open(email);

  }

}
