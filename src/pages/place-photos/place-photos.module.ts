import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { IonicImageLoader } from 'ionic-image-loader';

import { PlacePhotosPage } from './place-photos';

@NgModule({
  declarations: [
    PlacePhotosPage,
  ],
  imports: [
    IonicPageModule.forChild(PlacePhotosPage),
    IonicImageLoader
  ],
})
export class PlacePhotosPageModule {}
