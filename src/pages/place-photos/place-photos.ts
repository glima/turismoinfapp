import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ViewController } from 'ionic-angular';
import { ImageLoader, ImageLoaderConfig } from 'ionic-image-loader';
import { HttpHeaders } from '@angular/common/http';

import { RestTurismoProvider } from '../../providers/rest-turismo/rest-turismo';


@IonicPage()
@Component({
  selector: 'page-place-photos',
  templateUrl: 'place-photos.html',
})
export class PlacePhotosPage {

  photos: any;
  host: any = RestTurismoProvider.host + RestTurismoProvider.photoUrl;

  constructor(public navCtrl: NavController, public navParams: NavParams, public viewCtrl: ViewController,
    private imageLoaderConfig: ImageLoaderConfig) {
    this.photos = navParams.data;

    this.imageLoaderConfig.setHttpHeaders(
      new HttpHeaders().append('Authorization', 'Bearer ' + window.localStorage.getItem('logged'))
    );
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad PlacePhotosPage');
  }

  dismiss() {
    this.viewCtrl.dismiss();
  }

}
