import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ToastController } from 'ionic-angular';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

import { Loading } from '../../components/loading';
import { regexValidators } from '../validators/validator';
import { RestTurismoProvider } from '../../providers/rest-turismo/rest-turismo';


@IonicPage()
@Component({
  selector: 'page-change-pass',
  templateUrl: 'change-pass.html',
})
export class ChangePassPage {

  public credentialsForm: FormGroup;

  constructor(public navCtrl: NavController, public navParams: NavParams, public loading: Loading,
    private formBuilder: FormBuilder, private restTurismo: RestTurismoProvider, public toastCtrl: ToastController) {

      this.credentialsForm = this.formBuilder.group({

        current: [
          '',
          Validators.required
        ],
        newpass: [
          '',
          Validators.required
        ]
      });
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ChangePassPage');
  }

  changePass() {
    Loading.showLoading();
    this.restTurismo.changePass(
      { password: this.credentialsForm.controls['current'].value, newPassword: this.credentialsForm.controls['newpass'].value },
      (data) => {
        Loading.dismissLoading();
        this.credentialsForm.reset();
        this.showMessage(data);
      },
      (error) => {
        Loading.dismissLoading();
        this.showMessage(error.message);
      }
    );
  }

  showMessage(msg) {
    let toast = this.toastCtrl.create({
      message: msg,
      duration: 3000
    });
    toast.present();
  }

}
