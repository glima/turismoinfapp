import { Component } from '@angular/core';
import { IonicPage, NavController, ActionSheetController, MenuController } from 'ionic-angular';
import { HTTP } from '@ionic-native/http';

import { PlacePage } from '../place/place';
import { AuthPage } from '../auth/auth';
import { ChangePassPage } from '../change-pass/change-pass';
import { RestTurismoProvider } from '../../providers/rest-turismo/rest-turismo';
import { Loading } from '../../components/loading';

@IonicPage()
@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {

  config: any;

  constructor(public navCtrl: NavController, private http: HTTP, public actionSheetCtrl: ActionSheetController,
    private restTurismo: RestTurismoProvider, public loading: Loading, menu: MenuController) {
      this.config = { image: '', description: '' };
      this.loadConfig();
  }

  ionViewWillLeave() {
    Loading.dismissLoading();
  }

  loadConfig() {
    Loading.showLoading();

    this.restTurismo.getInitialConfig(
      (data) => {
        Loading.dismissLoading();
        this.config = data;

      },
      (error) => {
        Loading.dismissLoading();
      }
    );
  }
}
