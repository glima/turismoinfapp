import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { HTTP } from '@ionic-native/http';
import { ImageLoader, ImageLoaderConfig } from 'ionic-image-loader';
import { HttpHeaders } from '@angular/common/http';

import { EventDetailsPage } from '../event-details/event-details';
import { RestTurismoProvider } from '../../providers/rest-turismo/rest-turismo';
import { Loading } from '../../components/loading';

@IonicPage()
@Component({
  selector: 'page-event',
  templateUrl: 'event.html',
})
export class EventPage {

  events: any = [];
  offset: any;
  host: any = RestTurismoProvider.host + RestTurismoProvider.photoUrlEvent;

  constructor(public navCtrl: NavController, public navParams: NavParams, private http: HTTP,
    private restTurismo: RestTurismoProvider, public loading: Loading, private imageLoaderConfig: ImageLoaderConfig) {

      this.imageLoaderConfig.setHttpHeaders(
        new HttpHeaders().append('Authorization', 'Bearer ' + window.localStorage.getItem('logged'))
      );

      this.offset = 0;
      Loading.showLoading();
      this.listEvents();
  }

  ionViewWillLeave() {
    Loading.dismissLoading();
  }

  ionViewDidLoad() {
    Loading.dismissLoading();
  }

  showPhoto(img) {
    return img;
  }

  listEvents() {

    this.restTurismo.listEvents(this.offset,
      (data) => {
        Loading.dismissLoading();
        if(data.length > 0) {
          this.events = this.events.concat(data);
          this.offset = this.offset + 1;
        }

      },
      (error) => {
        Loading.dismissLoading();
      }
    );
  }

  doInfinite() {
    return new Promise((resolve) => {
      setTimeout(() => {
        this.listEvents();
        resolve();
      }, 1000);
    });
  }

  goEventDetails(event) {
    this.navCtrl.push('EventDetailsPage', {
      event: event
    });
  }

}
