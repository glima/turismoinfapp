import { Component } from '@angular/core';
import { IonicPage, NavController, ActionSheetController, MenuController } from 'ionic-angular';
import { HTTP } from '@ionic-native/http';
import { ImageLoader, ImageLoaderConfig } from 'ionic-image-loader';
import { HttpHeaders } from '@angular/common/http';

import { PlacePage } from '../place/place';
import { AuthPage } from '../auth/auth';
import { ChangePassPage } from '../change-pass/change-pass';
import { RestTurismoProvider } from '../../providers/rest-turismo/rest-turismo';
import { Loading } from '../../components/loading';

@IonicPage()
@Component({
  selector: 'page-category',
  templateUrl: 'category.html'
})
export class CategoryPage {

  categories: any;
  host: any = RestTurismoProvider.host + RestTurismoProvider.photoUrlCategory;

  constructor(public navCtrl: NavController, private http: HTTP, public actionSheetCtrl: ActionSheetController,
    private restTurismo: RestTurismoProvider, public loading: Loading, menu: MenuController,
    private imageLoaderConfig: ImageLoaderConfig, private imageLoader: ImageLoader) {

      this.imageLoaderConfig.setFallbackUrl('assets/imgs/category.svg');
      this.imageLoaderConfig.setHttpHeaders(
        new HttpHeaders().append('Authorization', 'Bearer ' + window.localStorage.getItem('logged'))
      );

      this.loadCategories();
  }

  ionViewWillLeave() {
    Loading.dismissLoading();
  }

  loadCategories() {
    Loading.showLoading();
    this.restTurismo.listCategories(
      (data) => {
        Loading.dismissLoading();

        this.categories = data;
      },
      (error) => {
        Loading.dismissLoading();
      }
    );
  }

  goPlaces(category) {
    this.navCtrl.push('PlacePage', {
      category: category
    });
  }

}
