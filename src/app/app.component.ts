import { Component, ViewChild } from '@angular/core';
import { Platform, MenuController, Nav } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';

import { HomePage } from '../pages/home/home';
import { ChangePassPage } from '../pages/change-pass/change-pass';
import { ContactPage } from '../pages/contact/contact';
import { CategoryPage } from '../pages/category/category';
import { AuthPage } from '../pages/auth/auth';

@Component({
  templateUrl: 'app.html'
})
export class MyApp {

  @ViewChild(Nav) nav: Nav;
  pages: Array<{title: string, component: any, icon: string}>;

  rootPage:any;

  constructor(platform: Platform, statusBar: StatusBar, splashScreen: SplashScreen, public menu: MenuController) {
    setTimeout(() => {
      platform.ready().then(() => {
        // Okay, so the platform is ready and our plugins are available.
        // Here you can do any higher level native things you might need.
        this.pages = [
          { title: 'Categorias', component: 'CategoryPage', icon: 'apps' },
          { title: 'Eventos', component: 'EventPage', icon: 'flag' },
          { title: 'Contato', component: 'ContactPage', icon: 'information' },
          { title: 'Altera Senha', component: 'ChangePassPage', icon: 'key' }
        ];

        this.menu.enable(true);
        statusBar.backgroundColorByHexString('#000000');
        this.checkPreviousAuthorization();
        splashScreen.hide();
      });
    }, 100);

  }

  checkPreviousAuthorization(): void {
    if((window.localStorage.getItem('logged') === "undefined" || window.localStorage.getItem('logged') === null)) {
      this.rootPage = 'AuthPage';
    } else {
      this.rootPage = 'HomePage';
    }
    //this.rootPage = HomePage;
  }

  openPage(page) {
    // Reset the content nav to have just this page
    // we wouldn't want the back button to show in this scenario
    this.menu.close();
    this.nav.push(page.component);
  }

  logout() {
    window.localStorage.setItem('logged', 'undefined');
    this.menu.close();
    this.nav.setRoot('AuthPage');
  }
}

