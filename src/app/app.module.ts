import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { SplashScreen } from '@ionic-native/splash-screen';
import { StatusBar } from '@ionic-native/status-bar';
import { HTTP } from '@ionic-native/http';
import { Facebook, FacebookLoginResponse } from '@ionic-native/facebook';
import { CallNumber } from '@ionic-native/call-number';
import { EmailComposer } from '@ionic-native/email-composer';
import { IonicImageLoader } from 'ionic-image-loader';

import { MyApp } from './app.component';
import { HomePage } from '../pages/home/home';
import { CategoryPage } from '../pages/category/category';
import { AuthPage } from '../pages/auth/auth';
import { PlacePage } from '../pages/place/place';
import { PlaceDetailsPage } from '../pages/place-details/place-details';
import { PlacePhotosPage } from '../pages/place-photos/place-photos';
import { ForgottenPassPage } from '../pages/forgotten-pass/forgotten-pass';
import { SignupPage } from '../pages/signup/signup';
import { ContactPage } from '../pages/contact/contact';
import { ChangePassPage } from '../pages/change-pass/change-pass';

import { RestTurismoProvider } from '../providers/rest-turismo/rest-turismo';
import { Loading } from '../components/loading';
import { LaunchNavigator } from '@ionic-native/launch-navigator';

@NgModule({
  declarations: [
    MyApp
  ],
  imports: [
    BrowserModule,
    IonicModule.forRoot(MyApp),
    IonicImageLoader.forRoot()
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp
  ],
  providers: [
    StatusBar,
    SplashScreen,,
    HTTP,
    Facebook,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    RestTurismoProvider,
    Loading,
    CallNumber,
    EmailComposer,
    LaunchNavigator
  ]
})
export class AppModule {}
