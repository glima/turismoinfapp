import { Injectable } from '@angular/core';
import { LoadingController } from 'ionic-angular';

import 'rxjs/add/operator/map';

@Injectable()
export class Loading {

    public static loader: any;
    public static loadingCtrl: LoadingController;

    constructor(loading: LoadingController) {
        Loading.loadingCtrl = loading;
    }

    static showLoading() {
      this.loader = this.loadingCtrl.create({
        content: 'Aguarde...',
        dismissOnPageChange: false
      });

      this.loader.present();
    }

    static dismissLoading() {
        if (this.loader) {
            this.loader.dismiss();
        }
    }

}
